<?php
ob_start();
session_start();
ini_set('display_errors', 0); 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Symbiosis</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
       <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
       <link rel="shortcut icon" href="images/fevicon_icon.ico" type="image/x-icon" />

<!-- Global site tag (gtag.js) - Google Analytics -->	
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63701310-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-63701310-1');
</script>	   
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

<!--HEADER GREY BACKGROUND CODE START HERE-->
<div class="header_color clearfix custom_mobile_class">
<div class="container">
<div class="custom_grey_heading clearfix">
	<div class="col-md-6 col-xs-6">
	<img src="images/whatsapp_white.png" class="img-responsive" style="display:inline-block;"><a class="white-text-heading" href="https://api.whatsapp.com/send?phone=918237011166"> +91 82370 11166</a>
	</div>
	<div class="col-md-6 col-xs-6">
	<img src="images/misscall_white.png" class="img-responsive" style="display:inline-block;"><a class="white-text-heading" href="tel:08048128800">080481 28800</a>
	</div>
</div>
</div>
</div>
<!--HEADER GREY BACKGROUND CODE END HERE-->

<!--BANNER START HERE-->
<nav id="nav" class="navbar navbar-inverse navbar-custom">
  <div class="container">

	<div class="div_helpline_number desktop_unique_section">
		<p class="top_helpline_number">
			<strong>Helpline Number:</strong> <a class="white-text white-text2" href="tel:02066211000">020 - 66211000</a>
		</p>
	</div>
	
    <div class="navbar-header">
     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    <!-- <a class="navbar-brand" href="#"><img src="http://www.scdl.net/images/SCDLLogo.png" class="img-responsive"></a>    -->
	
	<img src="http://www.scdl.net/images/SCDLLogo.png" class="img-responsive">
	
    </div>
    <div class="collapse navbar-collapse nav-margin" id="myNavbar">
      <ul class="nav navbar-nav navbar-bordered less-marg" style="margin-left:0%;" >
            <li class="active"><a class="white-text" href="#about">ABOUT PGDBA</a></li>
         <li><a class="white-text white-text2" href="#highlights">PROGRAM HIGHLIGHTS</a></li>
        <li><a class="white-text white-text2" href="#why">WHY SYMBIOSIS </a></li>
            <li><a class="white-text white-text2" href="#career" >FAST TRACK YOUR CAREER </a></li>
                <li><a class="white-text white-text2" href="#apply" style="border-right: 0px;">HOW TO APPLY</a></li>

          </ul>
      
	  <ul class="nav navbar-nav navbar-right less-marg custom_mobile_class2">
		<li><a class="white-text white-text2" href="https://api.whatsapp.com/send?phone=918237011166"><img src="images/whatsapp.png" class="img-responsive inline-block-class"> +91 82370 11166</a></li>
		<li><a class="white-text white-text2" href="tel:08048128800"><img src="images/misscall.png" class="img-responsive inline-block-class">080481 28800</a></li>
	  </ul>

	<div class="mobile_unique_section">
		<p class="top_helpline_number">
			<strong>Helpline Number:</strong> <a class="white-text white-text2" href="tel:02066211000">020 - 66211000</a>
		</p>
	</div>
	
    </div>
  </div>
</nav>

<div class="banner_pgdba clearfix">
	<div class="col-md-6">
	</div>
		<div class="col-md-6">
					<div class="our_program_fee margin_bottom_fee">
						<p>PROGRAM FEE: &#x20b9;46,000/-</p>				
					</div>		
					<div class="col-md-12">
						<img src="images/pgdba/Business-Administration_Text.png" class="img-responsive">
					</div>
					<div class="col-md-12 margin-top-class-schedule">
						<a class="smoothScroll" href="#bottom"><img src="images/schedulecall.png" class="img-responsive"></a>
					</div>
					<div class="col-md-12 margin-top-buttons">
						
							<a href="http://www.scdl.net/applyonline/SignUp.aspx?utm_campaign=<?php echo @$_GET['utm_campaign']; ?>&utm_medium=<?php echo @$_GET['utm_medium']; ?>&utm_content=<?php echo @$_GET['utm_content']; ?>&utm_display=<?php echo @$_GET['utm_display']; ?>&utm_keyword=<?php echo @$_GET['utm_keyword']; ?>&utm_adposition=<?php echo @$_GET['utm_adposition']; ?>&utm_placement=<?php echo @$_GET['utm_placement']; ?>&utm_creative=<?php echo @$_GET['utm_creative']; ?>&utm_source=<?php echo @$_GET['utm_source']; ?>" target="blank"><img src="images/applynow.png" class="img-responsive"></a>
					
					
							<a  class="smoothScroll1" href="#program_details"><img src="images/knowmore.png" class="img-responsive"></a>
									
					</div>				
				</div>
	
</div>
<section class="black-text-box clearfix">
<div class="container">
	<div class="row">
			
		<div class="col-md-12">
		
	<marquee behavior="scroll" direction="left" scrollamount="10">	ANNOUNCEMENTS | Admission open for AY 2020-21. | Limited seats available. | Apply Online Today to reserve your seat for programs at SCDL.</marquee>
			
		</div>		
	</div>
</div>
</section>
<!--BANNER END HERE-->

<!--AFTER BANNER INTRO SECTION START HERE-->
<section  id="about" class="section-p faq-section clearfix">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
		<h3 class="heading-name">PGDBA – POST GRADUATE DIPLOMA IN BUSINESS ADMINISTRATION</h3>
		<hr class="text_underline_about">
		<div class="col-md-12 centered margin_bottom50">
			<span class="program-heading">PGDBA DESIGNED FOR COMPETITIVE LEADERSHIP</span>	
		</div>
		<p class="centered line_height30">
		Our PGDBA is designed to create business leaders with cutting edge leadership skills and a sound knowledge base. 
		Professionals with a sharp business acumen that will help corporates exploit opportunities, meet business challenges 
		and steer them to growth and success through a competitive market place. It hones their analytical and problem solving skills to a keen edge, 
		under the expert guidance of eminent faculty and corporate stalwarts across industry. The result is a highly competent and 
		futuristic business leader thirsting to take on corporate challenges and provide organisations with innovative business solutions.			
		</p>				
	</div>
	</div>
	</div>
</section>
<!--AFTER BANNER INTRO SECTION END HERE-->

<!--PROGRAM HIGHLIGHTS SECTION START HERE-->
<section id="highlights" class="section-p redbox1">
<div class="container">
<div class="row">
		
			<h3>PROGRAM HIGHLIGHTS</h3>
			<hr class="text_underline margin_bottom50">	
			<ul class="line_height30">
				<li>The curriculum has been designed by a selected panel of business professionals and prominent academicians.</li>
				<li>Emphasis is on the contemporary issues including international commercial law and business environment.</li>
				<li>Learn India’s foreign trade along with international marketing trends to enter the demanding world of corporate management.</li>
				<li>Develops higher order of thinking skills in the area of International Business through theory and application based inputs and testing methodologies.</li>
			</ul>				
</div>
	
</div>
</section>
<!--PROGRAM HIGHLIGHTS SECTION END HERE-->

<!--PROGRAM DETAILS SECTION START HERE-->
<section id="program_details" class="section-p faq-section">
<div class="container">
	<div class="row">
		<h3>PROGRAM DETAILS</h3>
		<hr class="text_underline">		
	</div>
</div>
<div class="container">
	<div class="acc-panel">
    <div class="panel-group" id="accordion">
      
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    	ELIGIBILITY
					</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                	<!--<h4><strong>Criteria 1</strong></h4>-->
					<ul>		
						<li>Bachelor's degree holder / Graduate in any discipline from a recognised University.</li>
						<li>International / SAARC Graduate from a recognised / accredited University / Institution. </li>
						<li>Students who have appeared for final year of examination of their bachelor's degree program and are awaiting results can also apply, subject to successfully completing their bachelor's degree program / graduation within the time period specified by SCDL.</li>
					</ul>
				<!--</br>
                	<h4><strong>And Criteria 2</strong></h4>
					<ul>		
						<li>Students who have 3 years of work experience. OR</li>
						<li>Students who have qualified in SCDL - PGDBA Entrance Test. OR </li>
						<li>Students who have a valid score in any of the State Level MBA/ MMS-CET/ G-MAT/ N-MAT/ CMAT/ CAT/ MAT/ ATMA/ XAT or equivalent management entrance test at State or National level, valid for current academic year.</li>
					</ul>-->					
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    	FEES
                	</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
					<p><strong>Application Form Fees (Non-refundable):</strong></p>
					<ul>
					<li style="font-weight: 400;"><span style="font-weight: 400;">For Indian Residents (General / Defence Personnel / Paramilitary / Police): ₹ 1200/-</span></li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">International / SAARC: US $100 OR Rs. 7,200/-</span></li>
					</ul>
					<p><strong>Program Fees</strong></p>
					<ul>
					<li><span style="font-weight: 400;">General &amp; Defence: Rs.15,000</span></li>
					<li><span style="font-weight: 400;">International: US $350</span></li>
					<li><span style="font-weight: 400;">SAARC : US $300</span></li>
					</ul>
					<p>&nbsp;</p>


					</div>						
					</p>
					<p>
						<strong>Note:</strong>
						<ul>
							<li>Participants are required to pay additional exam fees per exam per attempt.</li>
						</ul>						
					</p>
					<p><strong>Fees can be paid Online or by Demand Draft.</strong></p>
					<ul>
						<li>Payment via Online: this can be done through payment gateway.</li>
						<li>Payment via DD: this can be submitted at SCDL Pune campus. The Demand Draft needs
						to be in favor of <b>"The Director, SCDL, Pune" payable at Pune</b> only.</li>
					</ul>																				
                </div>
            </div>
        </div>             

    </div>
</div>
</div>
</section>
<!--PROGRAM DETAILS SECTION END HERE-->

<!--WHY SYMBIOSIS SECTION START HERE-->
<section id="why" class="section-p redbox1">
<div class="container">
<div class="row">
		
			<h3>WHY SYMBIOSIS ?</h3>
			<hr class="text_underline margin_bottom50">	
			<ul class="line_height30">
				<li>One of best and largest autonomous distance learning institute in INDIA approved
				by AICTE.</li>
				<li> Over 350 adjunct faculty from top ranks of the industry for academic counselling,
				developing curriculum and assessments.</li>
				<li> Programs have high acceptance in Industry.</li>

				<li> An education provider to top corporates such as Wipro, EATON, Tata AIA,
				TAFE, Cipla, SBI General Insurance, IBM Vodafone and Many More...</li>
				<li> Blended Teaching and Learning methodology (Print, Online and Digital).</li>
				<li> Up to date Printed study books, online classroom learning, faculty interaction, e-
				mentoring and e-learning.</li>
			</ul>			
</div>
	
</div>
</section>
<!--WHY SYMBIOSIS SECTION END HERE-->

<!--FAST TRACK SECTION START HERE-->
<section id="career" class="section-p faq-section">
<div class="container">
<div class="row">
		
			<h3>FAST TRACK YOUR CAREER</h3>
			<hr class="text_underline margin_bottom50">	
			<p class="centered line_height30">
				The more qualifications you have, the faster you rise up the career path. Symbiosis
				Centre for Distance Learning helps you achieve prestigious qualifications without
				disrupting your job or home responsibilities. Its programs are specially structured in a
				way to add value to your qualifications and help you fast track your career. Symbiosis
				Centre for Distance Learning has been enhancing careers and fulfilling ambitions for
				more than four decades, helping people study while working and empowering them with
				the knowledge and confidence to rise up the professional ladder with ease.
			</p>			
</div>
	
</div>
</section>
<!--FAST TRACK SECTION END HERE-->

<!--HOW TO APPLY SECTION START HERE-->
<section id="apply" class="section-p">
<div class="container">
		<h3>HOW TO APPLY</h3>
	<hr class="text_underline margin_bottom50">
<div class="row">
	<div class="col-md-8">		
	<p>Visit www.scdl.net to Apply Online:</p>	
	<ul class="line_height30">
		<li> Purchase and fill application form online</li>
		<li> Submit copies of documents in prescribed format as per eligibility of applied
		program</li>
		<li> Pay program fee online through student login</li>
	</ul>
	</div>
	<div class="col-md-4">
		<a href="http://www.scdl.net/applyonline/SignUp.aspx?utm_campaign=<?php echo @$_GET['utm_campaign']; ?>&utm_medium=<?php echo @$_GET['utm_medium']; ?>&utm_content=<?php echo @$_GET['utm_content']; ?>&utm_display=<?php echo @$_GET['utm_display']; ?>&utm_keyword=<?php echo @$_GET['utm_keyword']; ?>&utm_adposition=<?php echo @$_GET['utm_adposition']; ?>&utm_placement=<?php echo @$_GET['utm_placement']; ?>&utm_creative=<?php echo @$_GET['utm_creative']; ?>&utm_source=<?php echo @$_GET['utm_source']; ?>" target="blank"><img src="images/applynow.png" class="img-responsive margin_top50 width50"></a>
	</div>		
</div>
	
</div>
</section>
<!--HOW TO APPLY SECTION END HERE-->
<hr style="border-top: 2px solid #f2e9e9;">
<!--TESTIMONIAL SECTION START HERE-->
<section class="section-p testimonial smoothScroll1" id="testimonial">
	<div class="container">
		<div class="row">
		<h3>TESTIMONIALS</h3>
		<hr class="text_underline">				
	</div>
	<div class="row section-p testimonial-p">
		<div class="col-md-2 testimonial-img-class">
			<img src="images/pgdba/piyush_chavda.jpg" class="img-responsive borderradius50">
		</div>
		<div class="col-md-8">
			<p class="testimonial-icon"><img src="images/quote.png" class="quote-img"></p>
			<p>I have enrolled for the PGDBA Program at Symbiosis Center for Distance Learning. 
						The elaborate course material, facility to have e-learning sessions, live chat room and not to forget 
						the virtual classrooms helps one to prepare in totality, thus enabling you to gain an in-depth understanding 
						of the subject. I feel most content and would like to appreciate the freedom that one gets to schedule their 
						exams online. The flexibility that the program offers to study at your own time, fits perfectly into my busy 
						work schedule in spite of spending long hours at the office. Above all, Symbiosis has a good recognition and 
						acceptability amongst employers and this helps one to progress in their career.</p>
			<br>
			<h5>Piyush Chavda,</h5>
			<h5>SME-UNIX</h5>
			<h5>HP India Sales P Ltd</h5>
		</div>
	</div>

	</div>
</section>
<!--TESTIMONIAL SECTION END HERE-->

<!--AWARDS SECTION START HERE-->
<section class="section-p faq-section">

<div class="container">
<div class="acc-panel">
    <div class="panel-group" id="accordion">
      
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                    	Awards
                	</a>
                </h4>
            </div>
            <div id="collapse8" class="panel-collapse collapse in">
                <div class="panel-body">
                	<ul>
                		<li>Symbiosis Centre for Distance Learning has been ranked 1st amongst the top Distance Learning Institutes.</li>
                		<li>Symbiosis Centre for Distance Learning has been ranked 2nd amongst the Leading Institutes.</li>
                		<li>Symbiosis Centre for Distance Learning has been ranked 3rd amongst the Top Distance Learning Institutes.</li>
                		<li>Symbiosis Centre for Distance Learning has been ranked 8th amongst the Best Distance Learning Institutes.</li>
                	</ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                    	Alumini association
                    </a>
                </h4>
            </div>
            <div id="collapse9" class="panel-collapse collapse">
                <div class="panel-body">
                	<ul>
                		<li>With over 10 lakh graduates, including leaders in corporate, nonprofit, and government organizations, we have the largest alumni network.</li>
                		<li>The Alumni Association has recently been established exclusively for graduates of SCDL programs.</li>
                		<li>The Alumni Association aims to form alumni groups worldwide that drive activity, organise events and networking opportunities in their local regions.</li>
                		<li>Our alumni play a crucial role in ongoing success and like any community, SCDL relies on its stakeholders, past and present, to play a part in its future.</li>
                	</ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
<!--AWARDS SECTION END HERE-->

<!--SCHEDULE A CALL FORM SECTION START HERE-->
<section>
	<div class="container">
		<div class="row">
	<div class="col-md-12 banner-form smoothScroll" id="bottom">
		
            		<h4 class="footer-form-heading">SCHEDULE A CALL</h4>
					<div id="messageDiv">
					  <?php
						//session_start();
						//ini_set('display_errors', 0); 
						 if($_SESSION['success']){
						 ?>
					  <div class="alert alert-success">      
						 <strong>Success ! </strong> Thank you for showing interest.
					  </div>
					  <?php  } 
						 if($_SESSION['error']){
						 ?>
					  <div class="alert alert-danger">
						 <strong>Alert ! </strong> Please enter all the fields.
					  </div>
					  <?php
						 }
						 unset($_SESSION['success']);unset($_SESSION['error']);
						 ?>
					</div>
		          <form role="form" method="post" action="../scdladmission2017/submit.php" id="contact-form" name="form" class="appointment-form">
		             <div class="row">
		                <div class="col-md-2">
		                   <div class="form-group">
		                      <input type="text" class="form-control form-inp requie" id="txtname" name="app_fname" placeholder="Name" required="">
		                   </div>
		               </div>
		          
		             
		           
		                <div class="col-md-2">
		                   <div class="form-group">
		                      <input type="text" class="form-control form-inp requie" id="txtemailid" name="app_email_address" placeholder="Email" required="">
		                   </div>
		                </div>			             	
		            
		           
		           		<div class="col-md-2" style="padding: 0px;">
		           			<div class="form-group ">
		           			<!-- <div class="col-md-8 col-xs-8">			               -->
			                      <input type="text" class="form-control form-inp requie" id="app_phone" name="app_phone" placeholder="Mobile" required="">
								  <!-- <label id="app_phone-error" class="error" for="app_phone" style="display:none; color: #ec1c23;margin-top: 5px;"></label> -->
			                   </div>
		                	
							<!-- </div> -->
		           		</div>
<!-- 
			            <div class="col-md-2">
			                   <div class="form-group">
			              <input type="text" class="form-control form-inp requie" id="app_opt" name="app_opt" placeholder="OTP" required="">
						  <label id="app_otp-error" class="error text-center" for="app_otp" style="display:none; color: #ec1c23;margin-top: 5px;"></label>
			                 </div>
			             </div>		 -->				

		                <div class="col-md-2">
		                   <div class="form-group">
		                      <input type="text" class="form-control form-inp requie" id="app_city" name="app_city" placeholder="City" required="">
		                   </div>
		                </div>
		           	            
								<input type="hidden" name="txtspeciality" value="Certificate Course in Virtual Workplace Management">					
						<input type="hidden" name="app_course" value="CCVWM" />	
		           		
		                <div class="col-md-8 col-xs-12">
		                      <div class="form-group">
		                         <textarea class="form-control" type="textarea" name="app_msg" placeholder="Your Message" maxlength="140" rows="1"></textarea>
		                      </div>
		                      <!--<button id="con_submit" class="site-btn text-center pull-right" type="submit">Submit</button>-->
		                   </div>
		                </div>
		           
		            
		             	<div class="col-md-12">
		             		<div class="form-group">
		             		 <label class="checkbox-inline">
      							<input checked type="checkbox" value=""> I authorize MD-SCDLTM representative to contact me. This will override registry on DND/NDNC.
   							 </label>
   							</div>

		             	</div>
		            
						<?php  require('../utm_parameters.php'); ?>
		             	<div class="col-md-12">
		             		<div class="form-group " align="left">
		             			  <button id="btnsubmit" name="btnsubmit" class="text-center" type="submit">SCHEDULE</button>
		             		</div>
		             	</div>
		             	</div>
		          </form>
            	</div>  
            	</div>  
            		</div> 
</section>
<!--SCHEDULE A CALL FORM SECTION END HERE-->

<!--FOOTER SECTION START HERE-->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				SYMBIOSIS CENTRE FOR DISTANCE LEARNING     |     Symbiosis Bhavan, 1065 B, Gokhale Cross Road, Model Colony, Pune - 411016, Maharashtra, India.
			</div>
			<div class="col-md-3">
				<span><a href="https://www.linkedin.com/company/symbiosis-centre-for-distance-learning/" style="color:#fff;" target="_blank"><i class="fa fa-linkedin"></i></a></span>
				<span><a href="https://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=SCDLSymbiosis" style="color:#fff;" target="_blank"><i class="fa fa-twitter"></i></a></span>
				<span><a href="https://www.facebook.com/SCDLDistanceLearning" style="color:#fff;" target="_blank"><i class="fa fa-facebook"></i></a></span>
			</div>
		</div>
	</div>
</footer>
<!--FOOTER SECTION END HERE-->

<style type="text/css">
.our_program_fee{
  background: #FF0000;
  width: 30%;
  text-align: center;
  color: #fff;
  font-weight: 700;
  padding: 6px 0px 0px 0px;
  border-radius: 5px;
  position: absolute;
  z-index: 999999;
  left: 554px;
  bottom: 428px;
  -moz-box-shadow: inset 0 0 10px #000000;
  -webkit-box-shadow: inset 0 0 10px #000000;
  box-shadow: inset 0 0 6px #000000;   
}

/* RESPONSIVE CSS
-------------------------------------------------- */
/* (1366x768) WXGA Display */
@media  screen and (max-width: 1366px) {
	.our_program_fee {
	    left: 460px;
	    bottom: 404px;
	    width: 32%;
	}  
}

@media (max-width: 667px) {
	.our_program_fee {
	    background: #ec1c23;
	    width: 60%;
	    text-align: center;
	    color: #fff;
	    font-weight: 700;
	    padding: 6px 0px 1px 0px;
	    border-radius: 5px;
	    position: unset;
	    z-index: 0;
	    left: 0px;
	    bottom: 0px;
	    -moz-box-shadow: inset 0 0 10px #000000;
	    -webkit-box-shadow: inset 0 0 10px #000000;
	    box-shadow: inset 0 0 10px #000000;
	    margin-bottom: 10px;
	    margin-left: 15px;
	} 
} 
</style>

<!--SCRIPTS SECTION START HERE-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/user_data_collector_form.js" type="text/javascript"></script>
<script src="../form_action.js" type="text/javascript"></script>
<script type="text/javascript">
	   /**
     * EFECTO PARA FLECHAS EN ACORDEON
     */
    
    $(document).on('show','.accordion', function (e) {
         //$('.accordion-heading i').toggleClass(' ');
         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
        //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });
</script>
<script type="text/javascript">
	$(function() {
  // This will select everything with the class smoothScroll
  // This should prevent problems with carousel, scrollspy, etc...
  $('.smoothScroll').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000); // The number here represents the speed of the scroll in milliseconds
        return false;
      }
    }
  });
});

</script>

<script type="text/javascript">
	$(function() {
  // This will select everything with the class smoothScroll
  // This should prevent problems with carousel, scrollspy, etc...
  $('.smoothScroll1').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000); // The number here represents the speed of the scroll in milliseconds
        return false;
      }
    }
  });
});

</script>
<script type="text/javascript">
$("#nav ul li a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});	
</script>
<!--SCRIPTS SECTION END HERE-->
<?php
##For other landing page  i.e.  Vertoz Side Wide
date_default_timezone_set('Asia/Calcutta'); 
$dateTime =  date("Y-m-d h:i:s");
?>  
<script type="text/javascript"> var page=encodeURI(window.location);var url='//cookie.vrtzads.com/pixel/vsync?convId=45588860074&label=Site%20Wide&order_id=<?= $dateTime ; ?>&order_value=100&rd=http://www.scdl.net/applyonline/SignUp.aspx&email=8237011166&product=PGDBA&u=nIS__6p5ZjRK96JSsqovYA&loc='+page;var vcp = document.createElement("img");vcp.setAttribute("src",url);vcp.setAttribute("height","1px");vcp.setAttribute("width","1px");document.body.appendChild(vcp);</script>
</body>
</html>